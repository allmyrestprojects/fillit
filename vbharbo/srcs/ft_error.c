/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/31 17:14:16 by vbharbo           #+#    #+#             */
/*   Updated: 2019/06/28 18:25:09 by bgerda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

int		ft_error(void)
{
	ft_putendl("error");
	return (0);
}

int		ft_error2(void)
{
	ft_putendl("open error");
	return (0);
}

void	ft_bleach(char c)
{
	int i;
	int j;

	i = -1;
	while (++i <= g_size_map)
	{
		j = -1;
		while (++j <= g_size_map)
		{
			if (c == g_field[i][j])
				g_field[i][j] = '_';
		}
	}
}

int		ft_size_map(void)
{
	int i;
	int j;

	i = g_sumt * 4;
	j = 1;
	while (j * j < i)
	{
		j++;
	}
	return (j);
}
