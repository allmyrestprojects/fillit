/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/31 13:10:48 by vbharbo           #+#    #+#             */
/*   Updated: 2019/06/28 21:07:15 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

static int	ft_putfile(char **av, char ***s)
{
	int		fd;
	int		ret;
	char	see[BUF + 1];

	fd = open(av[1], O_RDONLY);
	if (fd == -1)
		OPEN_ERROR;
	ret = BUF;
	if (!(s[0] = (char**)malloc(sizeof(char *) * 27)))
		ERROR;
	while (ret == BUF)
	{
		if ((ret = (int)read(fd, see, BUF)) == -1)
			ERROR;
		if (ret == 20)
			fd = -1;
/*		ft_putchar(see[20] + 'a');
		ft_putnbr(ret);*/
		see[ret] = '\0';
		if (ret)
			if (!(ft_determinate(see, &s[0])))
				return (0);
	}
	if (fd != -1)
		ERROR;
	return (1);
}

static void	ft_print_field(void)
{
	int	i;
	int	j;

	i = -1;
	while (++i < g_size_map)
	{
		j = -1;
		while (++j < g_size_map)
			if (g_field[i][j] == '_')
				ft_putchar('.');
			else
				ft_putchar(g_field[i][j]);
		ft_putchar('\n');
	}
}

int			main(int ac, char **av)
{
	char	**s;
	int		i;


	i = 0;
	g_sumt = 0;
	if (ac != 2)
		ft_putendl("usage : only one valid fillit file");
	else if (ft_putfile(av, &s))
	{
		ft_field(s);
		while (s[i])
			free(i++[s]);
		free(s);
		ft_print_field();
	}
	return (0);
}
