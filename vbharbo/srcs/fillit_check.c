/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_check.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/31 14:12:16 by vbharbo           #+#    #+#             */
/*   Updated: 2019/06/28 21:11:14 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

static int	ft_tetramino(char *see, char **s)
{
	int	i;
	int	j;

	i = -1;
	j = 0;
	if (!(s[0] = (char*)malloc(sizeof(char) * 4)))
		return (0);
	while (see[++i] && i < BUF - 1)
		if ((((i + 1) % 5) == 0) && (see[i] != '\n'))
			return (0);
		else if (see[i] != '.' && see[i] != '#' && ((i + 1) % 5) != 0)
			return (0);
		else if (see[i] == '#')
			s[0][j++] = i;
/*	ft_putnbr(i);
	ft_putchar('-');
*///ft_putnbr(j);
/*	ft_putendl("");
	ft_putendl(see);
	ft_putchar(see[i] + 'a');
	ft_putchar('\n' + 'a');
	ft_putendl("");*/
	if ((see[i] != '\0' && see[i] != '\n') || i != BUF - 1 || j != 4){
//		ft_putendl("hey");
		return (0);
	}
	return (1);
}

int			ft_love(char c, char b)
{

	if (c - 1 == b)
		return (1);
	if (c + 1 == b)
		return (1);
	if (c + 5 == b)
		return (1);
	if (c - 5 == b)
		return (1);
	return (0);
}

static int	ft_tetrocheck(char *s)
{
	int	i;
	int	j;
	int	k;

	i = -1;
	k = 0;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
			if (i != j && ft_love(s[i], s[j]))
				k++;
	}
	if (k == 6 || k == 8)
		return (1);
	else
		return (0);
}

static char *ft_tetronorm(char *s)
{
	while (s[0] > 4)
	{
		s[0] = s[0] - 5;
		s[1] = s[1] - 5;
		s[2] = s[2] - 5;
		s[3] = s[3] - 5;
	}
	while (s[0] != 0 && s[1] != 5 && s[2] != 10 && s[2] != 5)
	{
		s[0]--;
		s[1]--;
		s[2]--;
		s[3]--;
	}
	return (s);
}

/*static char	*ft_tetronorm(char *s)
{
	while (s[0] != 0 && s[1] != 5 && s[2] != 10 && s[2] != 5)
	{
		if (s[0] > 4)
		{
			s[0] = s[0] - 5;
			s[1] = s[1] - 5;
			s[2] = s[2] - 5;
			s[3] = s[3] - 5;
		}
		else
		{
			s[0]--;
			s[1]--;
			s[2]--;
			s[3]--;
		}
	}
	return (s);
}
*/
int			ft_determinate(char *see, char ***s)
{
	static int	*i;

	g_sumt++;
	if (g_sumt == 27)
		ERROR;
	if (!i)
	{
		i = (int*)malloc(sizeof(int) * 1);
		*i = 0;
	}
	if (!(ft_tetramino(see, *s + *i)))
		ERROR;
	if (!(ft_tetrocheck(s[0][*i])))
		ERROR;
	s[0][*i] = ft_tetronorm(s[0][*i]);
	*i = *i + 1;
	s[0][*i] = NULL;
	return (1);
}
