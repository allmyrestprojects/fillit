/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_field.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/02 14:33:21 by vbharbo           #+#    #+#             */
/*   Updated: 2019/06/28 17:37:50 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

static void	ft_create_field(void)
{
	int	i;
	int	j;

	i = -1;
	while (++i < 15)
	{
		j = -1;
		while (++j < 15)
			g_field[i][j] = '_';
	}
}

static void	ft_puttetro(char *s, int x, int y, char c)
{
	g_field[y + s[0] / 5][x + s[0] % 5] = c;
	g_field[y + s[1] / 5][x + s[1] % 5] = c;
	g_field[y + s[2] / 5][x + s[2] % 5] = c;
	g_field[y + s[3] / 5][x + s[3] % 5] = c;
}

static int	ft_tetro_can_you(char *s, int x, int y, char c)
{
	int x2;
	int y2;

	y2 = y + s[0] / 5;
	x2 = x + s[0] % 5;
	if (g_field[y2][x2] < c)
		return (0);
	y2 = y + s[1] / 5;
	x2 = x + s[1] % 5;
	if (g_field[y2][x2] < c || y2 >= g_size_map || x2 >= g_size_map)
		return (0);
	y2 = y + s[2] / 5;
	x2 = x + s[2] % 5;
	if (g_field[y2][x2] < c || y2 >= g_size_map || x2 >= g_size_map)
		return (0);
	y2 = y + s[3] / 5;
	x2 = x + s[3] % 5;
	if (g_field[y2][x2] < c || y2 >= g_size_map || x2 >= g_size_map)
		return (0);
	return (1);
}

static int	ft_recurse(char **s, int k)
{
	int	i;
	int j;

	i = -1;
	if (k == g_sumt)
		return (1);
	while (++i < g_size_map)
	{
		j = -1;
		while (++j < g_size_map)
			if (ft_tetro_can_you(s[k], j, i, 'A' + k))
			{
				ft_puttetro(s[k], j, i, 'A' + k);
				if (ft_recurse(s, k + 1))
					return (1);
				else
					ft_bleach(k + 'A');
			}
	}
	return (0);
}

void		ft_field(char **s)
{
	int	k;

	g_size_map = ft_size_map();
	ft_create_field();
	k = 0;
	while (!ft_recurse(s, k))
	{
		ft_create_field();
		g_size_map++;
	}
}
