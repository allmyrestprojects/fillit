/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 16:52:45 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/22 17:02:29 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*c;

	if (!s)
		return (NULL);
	c = (char*)malloc(sizeof(char) * (len + 1));
	if (!c)
		return (NULL);
	ft_strncpy(c, &s[start], len);
	c[len] = '\0';
	return (c);
}
