/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstmap.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/12 18:24:26 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/22 15:07:53 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*newlst;
	t_list	*relist;

	if (!lst || !f)
		return (NULL);
	newlst = f(lst);
	relist = newlst;
	while (lst->next)
	{
		lst = lst->next;
		newlst->next = f(lst);
		newlst = newlst->next;
	}
	return (relist);
}
