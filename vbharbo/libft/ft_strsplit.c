/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 18:27:46 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/22 18:32:19 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		str_or_not(char const *s, char c)
{
	if (*s != c)
		return (1);
	else
		return (0);
}

static int		str_symbols(char const *s, char c)
{
	int	count;

	count = 0;
	while (*s != c && *s != '\0')
	{
		s++;
		count++;
	}
	return (count);
}

static int		str_counter(char const *s, char c)
{
	int count;

	count = 0;
	while (*s)
	{
		if (*s != c && *s != '\0')
		{
			count++;
		}
		while (*s != c && *s != '\0')
			s++;
		if (*s == c)
			s++;
	}
	return (count);
}

static char		**ft_free(char ***str, size_t size)
{
	size_t	i;

	i = 0;
	if (str && *str)
	{
		while (i <= size)
		{
			if (str[0][i])
				free(str[0][i]);
			i++;
		}
		free(str[0]);
	}
	return (NULL);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**dst;
	int		i;
	int		j;
	int		size;

	i = 0;
	j = 0;
	if (!s)
		return (NULL);
	size = str_counter(s, c);
	if (!(dst = (char**)malloc(sizeof(char*) * (size + 1))))
		return (NULL);
	while (j < size)
	{
		while (!(str_or_not(&s[i], c)))
			i++;
		dst[j] = ft_strsub(s, i, str_symbols(&s[i], c));
		if (!dst[j])
			return (ft_free(&dst, j));
		i = i + str_symbols(&s[i], c);
		j++;
	}
	dst[size] = NULL;
	return (dst);
}
