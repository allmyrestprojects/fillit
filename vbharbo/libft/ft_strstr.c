/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 14:53:53 by vbharbo           #+#    #+#             */
/*   Updated: 2018/11/23 16:18:28 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	finder(const char *s1, const char *s2)
{
	if (*s2 == '\0')
		return (1);
	else if (*s2 == *s1)
		return (finder(&s1[1], &s2[1]));
	else
		return (0);
}

char		*ft_strstr(const char *haystack, const char *needle)
{
	size_t	i;
	char	*s;

	if (*needle == '\0' && *haystack == *needle)
		return ((char*)haystack);
	s = (char*)haystack;
	i = 0;
	while (s[i])
	{
		if (finder(&s[i], needle))
			return (&s[i]);
		i++;
	}
	return (NULL);
}
