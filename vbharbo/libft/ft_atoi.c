/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/22 13:59:29 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/22 16:12:52 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_over(int sign)
{
	if (sign == -1)
		return (0);
	else
		return (-1);
}

int			ft_atoi(const char *str)
{
	long long	result;
	int			sign;

	sign = 1;
	result = 0;
	while (*str == ' ' || *str == '\f' || *str == '\n' ||
			*str == '\v' || *str == '\t' || *str == '\r')
	{
		str++;
	}
	if (*str == '+')
		str++;
	else if (*str == '-')
	{
		str++;
		sign = -1;
	}
	while ((*str >= '0') && (*str <= '9'))
		if (result > result * 10 + (*str - '0'))
			return (ft_over(sign));
		else
			result = result * 10 + (*str++ - '0');
	return ((int)(result * sign));
}
