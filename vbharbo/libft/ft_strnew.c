/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strnew.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 16:01:09 by vbharbo           #+#    #+#             */
/*   Updated: 2019/02/08 14:11:16 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*c;

	if (size > size + 1)
		return (NULL);
	c = (char*)malloc(size + 1);
	if (!c)
		return (NULL);
	ft_bzero(c, size + 1);
	return (c);
}
