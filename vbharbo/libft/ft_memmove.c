/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memmove.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 18:31:29 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/22 15:08:39 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char	*dest;
	unsigned char	*sorc;
	size_t			i;

	dest = (unsigned char*)dst;
	sorc = (unsigned char*)src;
	i = -1;
	if (dst < src)
		while (++i < len)
			dest[i] = sorc[i];
	else if (dst > src)
	{
		i = len;
		while ((int)--i >= 0)
			dest[i] = sorc[i];
	}
	return (dst);
}
