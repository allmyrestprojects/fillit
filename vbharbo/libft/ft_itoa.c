/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/12 20:44:21 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/12 21:27:57 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static size_t	ft_len(int *n)
{
	size_t	len;
	int		pw;

	pw = 1;
	if (*n < 0)
	{
		*n = *n * -1;
		len = 2;
	}
	else
		len = 1;
	while (pw <= *n / 10)
	{
		pw = pw * 10;
		len++;
	}
	return (len);
}

char			*ft_itoa(int n)
{
	char	*s;
	size_t	len;

	if (n == 0)
		return (ft_strdup("0"));
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	len = ft_len(&n);
	s = (char*)malloc(sizeof(char) * (len + 1));
	if (!s)
		return (NULL);
	s[len] = '\0';
	s[0] = '-';
	while (n > 0)
	{
		s[--len] = n % 10 + '0';
		n = n / 10;
	}
	return (s);
}
