/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strrchr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 19:52:09 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/22 15:18:48 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	size_t	i;
	char	*b;

	i = -1;
	b = NULL;
	while (s[++i])
		if ((int)s[i] == c)
			b = (char*)&s[i];
	if ((int)s[i] == c)
		b = (char*)&s[i];
	return (b);
}
