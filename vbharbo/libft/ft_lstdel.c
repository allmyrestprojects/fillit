/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstdel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/12 18:24:10 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/22 15:05:42 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*list;

	if (alst && *alst && del)
	{
		list = *alst;
		while (list)
		{
			list = list->next;
			ft_lstdelone(alst, del);
			*alst = list;
		}
	}
}
