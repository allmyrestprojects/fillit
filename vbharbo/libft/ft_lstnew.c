/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstnew.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/12 18:25:16 by vbharbo           #+#    #+#             */
/*   Updated: 2019/01/22 15:46:25 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*list;

	list = malloc(sizeof(t_list));
	if (!list)
		return (NULL);
	list->next = NULL;
	list->content = NULL;
	list->content_size = 0;
	if (content)
	{
		list->content = (void*)malloc(content_size);
		ft_memcpy(list->content, content, content_size);
		list->content_size = content_size;
	}
	return (list);
}
