/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbharbo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/31 12:54:38 by vbharbo           #+#    #+#             */
/*   Updated: 2019/06/28 20:33:41 by vbharbo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "../libft/libft.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>

# define ERROR  return (ft_error())
# define OPEN_ERROR return (ft_error2())
# define BUF 21

char	g_field[15][15];
int		g_sumt;
int		g_size_map;

int		ft_determinate(char *see, char ***s);
int		ft_error(void);
int		ft_love(char c, char b);
int		ft_error2(void);
void	ft_field(char **s);
void	ft_bleach(char c);
int		ft_size_map(void);

#endif
